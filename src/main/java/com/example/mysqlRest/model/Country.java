package com.example.mysqlRest.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Immutable
@Table(name = "countries")
@Data
@EqualsAndHashCode(callSuper=true)
public class Country extends AbstractModel {
	@NotEmpty
	@Column(unique = true)
	private String code;

	@NotEmpty
	@Column(name = "dialling_code")
	private String diallingCode;

	@NotEmpty
	@Column(name = "label_lv")
	private String labelLv;

	@NotEmpty
	@Column(name = "label_en")
	private String labelEn;

	@NotEmpty
	@Column(name = "label_de")
	private String labelDe;

	@NotEmpty
	@Column(name = "label_lt")
	private String labelLt;

	@NotEmpty
	@Column(name = "label_ru")
	private String labelRu;

	@NotEmpty
	@Column(name = "label_et")
	private String labelEt;

	@NotEmpty
	@Column(name = "label_fi")
	private String labelFi;
}
